README

LaTeX thesis preamble include files

Created by Stephen Murphy on 2009-08-20

# Folder Contents

LaTeX includes for use with my standard thesis template, intended for dissertation and other academic/research writing.

These files are required by the template.tex file (in the latexthesis repo) and set most of the options needed.

Cloning these files into an `includes` directory in the texmf tree means that only the template.tex file is needed to start writing a document using the standard thesis settings/layout. Usually, this would mean:

    git clone git@bitbucket.org:stephenmurphy/thesispreamble.git ./includes/

The LaTeX document itself would also typically be under version control in its own repo.

Note that all other style files, BibTeX files etc would be in the texmf tree.

This folder is versioned with git.

# LaTeX Template usage example (snippet)

    %
    % LaTeX Template
    %
    % For UTS theses and assignments - by SM
    %

    ...


    \input{includes/preamble}
    
    ...


# License

All files are written by and copyright Stephen Murphy <mailto:stephen.j.murphy@student.uts.edu.au>, although there have been many influences over the years affecting the final result. The idea of the TODO indexes comes from Mark Eli Kalderon -- see this blog [post](http://markelikalderon.com/blog/2008/11/23/latex-todo/).

These files have been developed by the author for personal use, and are not in any way authorised by University of Technology, Sydney (UTS).

Any and all of the author's rights are currently reserved.

The inspiration for using git to version LaTeX documents/templates comes from Mark Eli Kalderon <mailto:eli@markelikalderon.com>. For more information, see this blog [post](http://markelikalderon.com/blog/2008/07/31/keeping-your-latex-preamble-in-a-git-submodule/).